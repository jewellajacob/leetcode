from helper_objects import ListNode


def create_linked_list(l1, end_node_pointer=-1):
    prehead = ListNode(-1)
    prev = prehead
    for num in l1:
        currNode = ListNode(num)
        prev.next = currNode
        prev = prev.next

    if end_node_pointer != -1:
        num = 0
        temp = prehead.next
        while num <= end_node_pointer:
            temp = temp.next
            num += 1
        prev.next = temp

    return prehead.next


def print_linked_list_values(l1):
    while l1:
        print(l1.val)
        l1 = l1.next
