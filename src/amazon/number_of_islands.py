"""
Leetcode Number: 200

runtime:
space:
"""

class Solution:
    def numIslands(self, grid) -> int:

        total = 0
        for x in range(0, len(grid)):
            for y in range(0, len(grid[0])):
                if grid[x][y] == '1':
                    self.dfs(grid, x, y)
                    total += 1
        return total

    def dfs(self, grid, x, y):
        if x < 0 or x > len(grid)-1 or y < 0 or y > len(grid[0])-1 or grid[x][y] != '1':
            return

        grid[x][y] = '0'
        self.dfs(grid, x+1, y)
        self.dfs(grid, x-1, y)
        self.dfs(grid, x, y+1)
        self.dfs(grid, x, y-1)