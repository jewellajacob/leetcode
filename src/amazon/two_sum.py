"""
Leetcode Number: 1

Notes:
    enumerate is slightly faster than range: https://stackoverflow.com/questions/4852944/what-is-faster-for-loop-using-enumerate-or-for-loop-using-xrange-in-python
"""


class Solution():
    def two_sum(self, nums, target):
        sum_dict = {}
        for index, num in enumerate(nums):
            if num in sum_dict:
                return [sum_dict[num], index]
            else:
                sum_dict[target - num] = index
