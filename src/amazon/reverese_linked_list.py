"""
Problem: 206
Run Time:
Space Complexity:
"""

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        curr = head
        values = []
        while curr != None:
            values.append(curr.val)
            curr = curr.next

        new_list = None
        new_head = None
        for ind in range(len(values) - 1, -1, -1):
            if new_list == None:
                new_list = ListNode(values[ind])
                new_head = new_list
            else:
                new_list.next = ListNode(values[ind])
                new_list = new_list.next

        return new_head