"""
Problem: Pacific Atlantic Water Flow
Link: https://leetcode.com/problems/linked-list-cycle
Run Time: O(N)
Space Complexity: O(N)
"""

from helper_objects import ListNode


class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        visited = set()

        while head:
            if head in visited:
                return True
            visited.add(head)
            head = head.next

        return False


