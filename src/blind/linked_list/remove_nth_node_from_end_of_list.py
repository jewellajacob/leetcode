"""
Problem: Remove Nth Node from End of List
Link: https://leetcode.com/problems/remove-nth-node-from-end-of-list/
Run Time: O(n)
Space Complexity: O(1)
"""

from helper_objects import ListNode


class Solution:
    def remove_nth_from_end(self, head, n: int):
        hold_node = ListNode()
        hold_node.next = head  # assigninng to .next is necessary for cases of 1 node

        # finds length of list, for [1,2,3,4,5] length=5
        temp_node = head
        length = 0
        while temp_node:
            length += 1
            temp_node = temp_node.next

        # iterate to the node right before removal
        temp_node = hold_node
        length -= n  # length = 3 if n =2
        while length > 0:
            temp_node = temp_node.next
            length -= 1

        # replace the n node. in case of [1] => temp_node =[0] .next = [1] .next.next = None => hold.next = None
        temp_node.next = temp_node.next.next

        return hold_node.next
