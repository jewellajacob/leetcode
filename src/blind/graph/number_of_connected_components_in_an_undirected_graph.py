"""
Problem: Number of Connected Components in an Undirected Graph
Link: https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/
Run Time:
Space Complexity:
"""

class Solution:
    def countComponents(self, n, edges) -> int:

        graphs = 0
        connections = {x:[] for x in range(n)}
        visited = [0] * n
        for edge in edges:
            connections[edge[0]].append(edge[1])
            connections[edge[1]].append(edge[0])

        for vert in range(n):
            if not visited[vert]:
                self.dfs(vert, connections, visited)
                graphs += 1
        return graphs

    def dfs(self, start_vert, connections, visited):
        if visited[start_vert]:
            return
        visited[start_vert] = 1
        for x in connections[start_vert]:
            self.dfs(x, connections, visited)
