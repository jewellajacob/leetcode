"""
Problem: Longest Consecutive Sequence
Link: https://leetcode.com/problems/longest-consecutive-sequence/solution/
Run Time: o(n)
Space Complexity: o(n)
"""


class Solution:
    def longestConsecutive(self, nums) -> int:
        longest = 0
        nums = set(nums)

        for num in nums:
            if num - 1 not in nums:
                curr_longest = 1
                curr_num = num

                while curr_num + 1 in nums:
                    curr_num = curr_num + 1
                    curr_longest += 1

                longest = max(longest, curr_longest)

        return longest
