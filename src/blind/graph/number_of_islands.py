"""
Problem: Number of Islands
Link: https://leetcode.com/problems/number-of-islands/solution/
Run Time: O(M*N)
Space Complexity:
"""

class Solution:
    def numIslands(self, grid) -> int:

        total = 0

        for row in range(len(grid)):
            for col in range(len(grid[0])):
                if grid[row][col] == "0":
                    continue
                else:
                    self.dfs(grid, row, col)
                    total += 1

        return total

    def dfs(self, grid, row, col):

        if row < 0 or row > (len(grid) - 1) or col < 0 or col > (len(grid[0]) - 1) or grid[row][col] == "0":
            return

        grid[row][col] = "0"

        self.dfs(grid, row + 1, col)
        self.dfs(grid, row - 1, col)
        self.dfs(grid, row, col + 1)
        self.dfs(grid, row, col - 1)
