"""
Problem: Pacific Atlantic Water Flow
Link: https://leetcode.com/problems/pacific-atlantic-water-flow/
Run Time: O(M*N)
Space Complexity: O(M*N)
"""


class Solution:
    def pacificAtlantic(self, heights):
        if not heights:
            return []

        # setup
        p_visited = set()
        a_visited = set()

        row_length = len(heights)
        col_length = len(heights[0])

        directions = ((0, 1), (1, 0), (0, -1), (-1, 0))

        # dfs
        def dfs(row, col, visited):
            if (row, col) in visited:
                return
            visited.add((row, col))

            for row_dir, col_dir in directions:
                next_row = row + row_dir
                next_col = col + col_dir
                if 0 <= next_row < row_length and 0 <= next_col < col_length:
                    if heights[next_row][next_col] >= heights[row][col]:
                        dfs(next_row, next_col, visited)

        # navigate the walls of the island
        for position in range(row_length):
            dfs(position, 0, p_visited)
            dfs(position, col_length - 1, a_visited)

        for position in range(col_length):
            dfs(0, position, p_visited)
            dfs(row_length - 1, position, a_visited)

        return list(p_visited & a_visited)