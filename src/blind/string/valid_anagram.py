"""
Problem: Remove Nth Node from End of List
Link: https://leetcode.com/problems/valid-anagram/
Run Time: O(s+t)
Space Complexity: O(s)
"""
import string


class Solution:
    def is_anagram(self, s: str, t: str) -> bool:
        s_len = len(s)
        t_len = len(t)
        s_letters = dict.fromkeys(string.ascii_lowercase, 0)

        if s_len != t_len:
            return False

        for letter in s:
            s_letters[letter] += 1

        for letter in t:
            if s_letters[letter] == 0:
                return False
            else:
                s_letters[letter] -= 1

        for key, value in s_letters.items():
            if value != 0:
                return False

        return True