"""
Problem: Subtree of Another Tree
Link: https://leetcode.com/problems/subtree-of-another-tree
Run Time:
Space Complexity:
"""


class Solution:
    def isSubtree(self, root, subRoot) -> bool:
        def convert(tree):
            return '^' + str(tree.val) + '#' + convert(tree.left) + '#' + convert(tree.right) if tree else '$'

        return convert(subRoot) in convert(root)
