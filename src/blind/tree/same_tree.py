"""
Problem: Subtree of Another Tree
Link: https://leetcode.com/problems/same-tree
Run Time:
Space Complexity:
"""


class Solution:
    def is_same_tree(self, p, q) -> bool:
        def convert_tree_to_string(root):
            return '$' + str(root.val) + '@' + convert_tree_to_string(root.left) + '@' + convert_tree_to_string(
                root.right) if root else '#'

        return True if convert_tree_to_string(p) == convert_tree_to_string(q) else False
