"""
Problem: Subtree of Another Tree
Link: https://leetcode.com/problems/longest-increasing-subsequence/
Run Time:
Space Complexity:
"""


class Solution:
    def length_of_LIS(self, nums) -> int:
        dp = [1] * len(nums)  # saves largest sequence for each index of nums
        for i in range(1, len(nums)):
            for j in range(i):
                if nums[i] > nums[j]:
                    dp[i] = max(dp[i], dp[j] + 1)
        return max(dp)
