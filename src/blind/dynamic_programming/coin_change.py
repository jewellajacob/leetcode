"""
Problem: Subtree of Another Tree
Link: https://leetcode.com/problems/coin-change
Run Time: O(S∗n) coins * amount
Space Complexity: O(S) s = memo table
"""


class Solution:
    def coin_change(self, coins, amount: int) -> int:
        dp = [float('inf')] * (
                amount + 1)  # dp[x] is used to store the current number of coins needed to create value x
        dp[0] = 0  # used as base

        for coin in coins:
            for x in range(coin, amount + 1):
                dp[x] = min(dp[x], dp[
                    x - coin] + 1)  # what is smaller, the current stored number of coins to make x or the stored value minus the value of the current coin plus 1 for the current coin

        return dp[amount] if dp[amount] != float('inf') else -1  # catch if no coins make value
