"""
Problem: Climbing Stairs
Link: https://leetcode.com/problems/climbing-stairs
Run Time: O(n)
Space Complexity: O(n)
"""


class Solution:
    def climb_stairs(self, n: int) -> int:
        if n == 1: return 1
        dp = [0] * (n + 1)
        dp[0], dp[1], dp[2] = 0, 1, 2

        for index in range(3, n + 1):
            dp[index] = dp[index - 1] + dp[index - 2]

        return dp[n]