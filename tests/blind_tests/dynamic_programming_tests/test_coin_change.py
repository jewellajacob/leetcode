import unittest

from src.blind.dynamic_programming.coin_change import Solution


class TestCoinChange(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestCoinChange, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        coins = [1,2,5]
        amount = 11
        expected_output = 3

        actual_output = self.solution.coin_change(coins, amount)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        coins = [2]
        amount = 3
        expected_output = -1

        actual_output = self.solution.coin_change(coins, amount)

        self.assertEqual(actual_output, expected_output)

    def test_example_3(self):
        coins = [1]
        amount = 0
        expected_output = 0

        actual_output = self.solution.coin_change(coins, amount)

        self.assertEqual(actual_output, expected_output)

    def test_example_4(self):
        coins = [1]
        amount = 1
        expected_output = 1

        actual_output = self.solution.coin_change(coins, amount)

        self.assertEqual(actual_output, expected_output)

    def test_example_5(self):
        coins = [1]
        amount = 2
        expected_output = 2

        actual_output = self.solution.coin_change(coins, amount)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()