import unittest

from src.blind.dynamic_programming.climbing_stairs import Solution


class TestClimbingStairs(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestClimbingStairs, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        input = 2
        expected_output = 2

        actual_output = self.solution.climb_stairs(input)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        input = 3
        expected_output = 3

        actual_output = self.solution.climb_stairs(input)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()