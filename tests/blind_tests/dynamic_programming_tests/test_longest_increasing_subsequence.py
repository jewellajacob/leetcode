import unittest

from src.blind.dynamic_programming.longest_increasing_subsequence import Solution


class TestLongestIncreasingSubsequence(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestLongestIncreasingSubsequence, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        nums = [10, 9, 2, 5, 3, 7, 101, 18]
        expected_output = 4

        actual_output = self.solution.length_of_LIS(nums)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        nums = [0, 1, 0, 3, 2, 3]
        expected_output = 4

        actual_output = self.solution.length_of_LIS(nums)

        self.assertEqual(actual_output, expected_output)

    def test_example_3(self):
        nums = [7, 7, 7, 7, 7, 7, 7]
        expected_output = 1

        actual_output = self.solution.length_of_LIS(nums)

        self.assertEqual(actual_output, expected_output)


if __name__ == '__main__':
    unittest.main()
