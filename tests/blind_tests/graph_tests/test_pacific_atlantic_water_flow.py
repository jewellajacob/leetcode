import unittest

from src.blind.graph.pacific_atlantic_water_flow import Solution


class TestPacificAtlanticWaterFlow(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestPacificAtlanticWaterFlow, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        heights = [[1, 2, 2, 3, 5], [3, 2, 3, 4, 4], [2, 4, 5, 3, 1], [6, 7, 1, 4, 5], [5, 1, 1, 2, 4]]
        expected_output = [(4, 0), (0, 4), (3, 1), (1, 4), (3, 0), (2, 2), (1, 3)]

        actual_output = self.solution.pacificAtlantic(heights)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        heights = [[2, 1], [1, 2]]
        expected_output = [(0, 1), (1, 0), (1, 1), (0, 0)]

        actual_output = self.solution.pacificAtlantic(heights)

        self.assertEqual(actual_output, expected_output)


if __name__ == '__main__':
    unittest.main()
