import unittest

from src.blind.graph.longest_consecutive_sequence import Solution

class TestLongestConsecutiveSequence(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestLongestConsecutiveSequence, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        nums = [100,4,200,1,3,2]
        expected_output = 4

        actual_output = self.solution.longestConsecutive(nums)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        nums = [0,3,7,2,5,8,4,6,0,1]
        expected_output = 9

        actual_output = self.solution.longestConsecutive(nums)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()
