import unittest

from src.blind.graph.number_of_islands import Solution


class TestNumberOfIslands(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestNumberOfIslands, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        grid = [
            ["1", "1", "1", "1", "0"],
            ["1", "1", "0", "1", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "0", "0", "0"]
        ]
        expected_output = 1

        actual_output = self.solution.numIslands(grid)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        grid = [
            ["1", "1", "0", "0", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "1", "0", "0"],
            ["0", "0", "0", "1", "1"]
        ]
        expected_output = 3

        actual_output = self.solution.numIslands(grid)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()
