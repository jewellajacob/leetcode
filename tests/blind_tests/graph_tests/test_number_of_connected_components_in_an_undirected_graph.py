import unittest

from src.blind.graph.number_of_connected_components_in_an_undirected_graph import Solution

class TestNumberOfConnectedComponentsInAnUndirectedGraph(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestNumberOfConnectedComponentsInAnUndirectedGraph, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        n = 5
        edges = [[0,1],[1,2],[3,4]]
        expected_output = 2

        actual_output = self.solution.countComponents(n, edges)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        n = 5
        edges = [[0,1],[1,2],[2,3],[3,4]]
        expected_output = 1

        actual_output = self.solution.countComponents(n, edges)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()
