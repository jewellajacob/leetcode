import unittest

from src.blind.string.valid_anagram import Solution


class TestValidAnagram(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestValidAnagram, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        s = "anagram"
        t = "nagaram"
        expected_output = True

        actual_output = self.solution.is_anagram(s, t)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        s = "car"
        t = "rat"
        expected_output = False

        actual_output = self.solution.is_anagram(s, t)

        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()
