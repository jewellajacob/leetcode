import unittest

from src.blind.linked_list.remove_nth_node_from_end_of_list import Solution
from helper_functions import create_linked_list


class TestRemoveNthNodeFromEndOfList(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestRemoveNthNodeFromEndOfList, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        head = create_linked_list([1,2,3,4,5])
        n = 2
        expected_output = create_linked_list([1,2,3,5])

        actual_output = self.solution.remove_nth_from_end(head, n)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next

    def test_example_2(self):
        head = create_linked_list([1, 2])
        n = 1
        expected_output = create_linked_list([1])

        actual_output = self.solution.remove_nth_from_end(head, n)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next


    def test_example_3(self):
        head = create_linked_list([1])
        n = 1
        expected_output = create_linked_list([])

        actual_output = self.solution.remove_nth_from_end(head, n)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next


if __name__ == '__main__':
    unittest.main()
