import unittest

from src.blind.linked_list.linked_list_cycle import Solution
from helper_functions import create_linked_list


class TestLinkedListCycle(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestLinkedListCycle, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        head = create_linked_list([3,2,0,-4], 1)
        expected_output = True

        actual_output = self.solution.hasCycle(head)

        self.assertEqual(actual_output, expected_output)

    def test_example_2(self):
        head = create_linked_list([1,2], 0)
        expected_output = True

        actual_output = self.solution.hasCycle(head)

        self.assertEqual(actual_output, expected_output)

    def test_example_3(self):
        head = create_linked_list([1], -1)
        expected_output = False

        actual_output = self.solution.hasCycle(head)

        self.assertEqual(actual_output, expected_output)


if __name__ == '__main__':
    unittest.main()
