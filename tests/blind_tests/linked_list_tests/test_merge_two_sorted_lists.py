import unittest

from src.blind.linked_list.merge_two_sorted_lists import Solution
from helper_functions import create_linked_list


class TestMergeTwoSortedLists(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestMergeTwoSortedLists, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        l1 = create_linked_list([1,2,4])
        l2 = create_linked_list([1,3,4])
        expected_output = create_linked_list([1,1,2,3,4,4])

        actual_output = self.solution.mergeTwoLists(l1, l2)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next

    def test_example_2(self):
        l1 = create_linked_list([])
        l2 = create_linked_list([])
        expected_output = create_linked_list([])

        actual_output = self.solution.mergeTwoLists(l1, l2)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next

    def test_example_3(self):
        l1 = create_linked_list([])
        l2 = create_linked_list([0])
        expected_output = create_linked_list([0])

        actual_output = self.solution.mergeTwoLists(l1, l2)

        while actual_output:
            self.assertEqual(actual_output.val, expected_output.val)
            actual_output = actual_output.next
            expected_output = expected_output.next


if __name__ == '__main__':
    unittest.main()

