import unittest
from src.amazon.number_of_islands import Solution


class TestNumberOfIslands(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestNumberOfIslands, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_example_1(self):
        grid = [
            ["1", "1", "1", "1", "0"],
            ["1", "1", "0", "1", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "0", "0", "0"]
        ]
        target = 1

        actual = self.solution.numIslands(grid)
        self.assertEqual(actual, target)

    def test_example_2(self):
        grid = [
            ["1", "1", "0", "0", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "1", "0", "0"],
            ["0", "0", "0", "1", "1"]
        ]
        target = 3

        actual = self.solution.numIslands(grid)
        self.assertEqual(actual, target)