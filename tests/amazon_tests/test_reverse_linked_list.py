import unittest

from src.amazon.reverese_linked_list import ListNode, Solution


class TestReverseLinkedList(unittest.TestCase):
    def test_base_success(self):
        sol = Solution()
        first_list = [1, 2, 3, 4, 5]
        expected_list = ListNode(first_list[len(first_list) - 1])
        expected_head = expected_list
        head = ListNode(first_list[0])
        node_list = head

        for ind in range(len(first_list) - 2, -1, -1):
            expected_list.next = ListNode(first_list[ind])
            expected_list = expected_list.next
        for ind in range(1, len(first_list)):
            node_list.next = ListNode(first_list[ind])
            node_list = node_list.next

        actual_head = sol.reverseList(head)

        while expected_head != None:
            self.assertEqual(actual_head.val, expected_head.val)
            expected_head = expected_head.next
            actual_head = actual_head.next


if __name__ == '__main__':
    unittest.main()
