import unittest
from src.amazon.two_sum import Solution

from src.amazon import two_sum


class TestTwoSum(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestTwoSum, self).__init__(*args, **kwargs)
        self.solution = Solution()

    def test_success(self):
        nums = [2,7,11,15]
        target = 9
        expected_output = [0,1]

        actual_output = self.solution.two_sum(nums, target)

        self.assertEqual(actual_output, expected_output)

    def test_two_equal_nums(self):
        nums = [3,3]
        target = 6
        expected = [0,1]

        actual = self.solution.two_sum(nums, target)

        self.assertEqual(actual, expected)

    def test_with_zero_in_answer(self):
        nums = [0, 1, 4, 3]
        target = 3
        expected = [0, 3]

        actual = self.solution.two_sum(nums, target)

        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
