import unittest
from collections import OrderedDict

from src.amazon.lru_cache import LRUCache


class TestLRUCache(unittest.TestCase):
    def test_base_success(self):
        commands = ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
        related_number = [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
        expected = OrderedDict([(3, 3), (4, 4)])

        cache = LRUCache(related_number[0][0])
        for index in range(1, len(commands)):
            if commands[index] == "put":
                cache.put(related_number[index][0], related_number[index][0])
            elif commands[index] == "get":
                cache.get(related_number[index][0])

        self.assertEqual(cache.cache, expected)

if __name__ == '__main__':
    unittest.main()